#!/bin/sh
cat <<EOF > /usr/share/nginx/html/configuration.json
{
	"URL_API": "${API_URL}"
}
EOF

exec "$@"
