import React, { useEffect, useState } from "react";
import "./App.css";
import { getConfig } from "./config-utils";
import logo from "./logo.svg";

const fetchStatus = async (setStatus) => {
  fetch((await getConfig()).URL_API + "/actuator/health").then((response) => {
    response.json().then((json) => {
      console.log(json);
      if (response.status === 200) {
        setStatus({ text: json.status });
      } else {
        setStatus({ text: "FAILED" });
      }
    });
  });
};

const fetchText = async (setText) => {
  fetch((await getConfig()).URL_API + "/person/1").then((response) => {
    response.json().then((json) => {
      if (response.status === 200) {
        const data = {};
        data.text =
          "working u are actually seeing : " +
          json.id +
          " " +
          json.lastName +
          " " +
          json.firstName;
        setText(data);
      } else {
        setText({ text: "Erreur sur les données" });
      }
    });
  });
};

function App() {
  const [status, setStatus] = useState({ text: "UNKNOWN" });

  const [hello, setHello] = useState({
    text: "rien, pas connecté aux api !!!",
  });

  useEffect(() => {
    fetchStatus(setStatus);
  }, []);
  useEffect(() => {
    fetchText(setHello);
  }, []);

  return (
    <div className="App">
      <h1>Modif 1</h1>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        Etat des API : {status.text} <br />
        Que disent les api ? {hello.text}
      </header>
    </div>
  );
}

export default App;
