package fr.valbat.applicationback;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Person {

	@Id
	@GeneratedValue(generator = "sequence-generator")
	private Long id;

	@Column
	private String lastName;

	@Column
	private String firstName;

}
