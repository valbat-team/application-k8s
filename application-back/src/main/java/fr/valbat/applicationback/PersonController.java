package fr.valbat.applicationback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {

	private Logger logger = LoggerFactory.getLogger(PersonController.class);

	@Autowired
	private PersonRepository personRepository;

	@GetMapping("/person/{id}")
	@CrossOrigin(origins = "*")
	public Person getPerson(@PathVariable Long id) {
		logger.info("personId={}", id);
		return personRepository.findById(id).get();
	}
}
